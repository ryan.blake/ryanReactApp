import React from 'react';
import {connectWithLifecycle} from 'react-lifecycle-component';
import actions from '../../actions/simpleAction';

import '../../App.css';

const Home = ({result, onClick}) => (
  <div className='container'>
    <div className='row'>
        <p>{result}</p>
      <button onClick={onClick}>Do State Stuff</button>
    </div>
  </div>
);

const mapStateToProps = ({ simple }) => ({
  result: simple.result
});

const mapDispatchToProps = dispatch => ({
  onClick: () =>{
    dispatch(actions.simple());
  }  
});

export default connectWithLifecycle(mapStateToProps, mapDispatchToProps)(Home);
