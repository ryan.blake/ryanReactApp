import React from 'react';
import { Route } from 'react-router';
import  NavMenu  from './components/common/NavMenu';
import Home from './components/home/index';

export default () => (
  <div>
    <NavMenu />
    <div className='container'>
      <Route exact path='/' component={Home} /> 
    </div>
  </div>
);
