import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { MemoryRouter } from 'react-router-dom';
import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const state = { simple: {} };
  const storeMock = (state) => ({
    default: () => {},
    subscribe: () => {},
    dispatch: () => {},
    getState: () => ({...state})
  });

  ReactDOM.render(
    <Provider store={storeMock(state)}>
      <MemoryRouter>
        <App />
      </MemoryRouter>
    </Provider>, div
  );
  ReactDOM.unmountComponentAtNode(div);
});
